# Mariadb role

This role covers deployment, configuration and software updates of MariaDB Server. This role is a fork of [https://github.com/deimosfr/ansible-mariadb](https://github.com/deimosfr/ansible-mariadb) vy **Pierre Mavro / deimosfr** and is released under GPL2 Licence. We give no warranty for this piece of software. Currently supported OS - Debian..


